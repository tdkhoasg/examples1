package com.example.tdkhoa.myapplication

import android.app.AlertDialog
import android.content.*
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.MenuItemCompat
import android.support.v7.widget.ShareActionProvider
import android.util.Log
import android.view.Menu
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.JsonHttpResponseHandler
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.net.URLEncoder

public class MainActivity2 : AppCompatActivity() {

    var jsonAdapter: JSONAdapter? = null
    var sharedPreferences: SharedPreferences? = null
    private var shareActionProvider: ShareActionProvider? = null
    private val PREFS: String = "prefs"
    private val PREF_NAME: String = "name"
    private val QUERY_URL: String = "http://openlibrary.org/search.json?q="
    private var name: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        displayWelcome()

        main_button.setOnClickListener {
            val keyword = main_editext.text.toString()
            queryBooks(keyword)
            val inputManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(
                currentFocus!!.windowToken, InputMethodManager.HIDE_NOT_ALWAYS
            )
        }


        main_listview.setOnItemClickListener { parent, view, position, id ->
            val coverID = jsonAdapter?.getItem(position)?.optString("cover_i", "")
//            val intent = Intent(this, DetailActivity::class.java)
            intent.putExtra("coverID", coverID)
            startActivity(intent)

        }


    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {

        // Inflate the menu.
        // Adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)

        // Access the Share Item defined in menu XML
        val shareItem = menu.findItem(R.id.menu_item_share)

        // Access the object responsible for
        // putting together the sharing submenu
        if (shareItem != null) {
            shareActionProvider = MenuItemCompat.getActionProvider(shareItem) as ShareActionProvider
        }

        // Create an Intent to share your content
        setShareIntent()

        return true
    }

    private fun setShareIntent() {

        // create an Intent with the contents of the TextView
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.type = "text/plain"
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Android Development")
        shareIntent.putExtra(Intent.EXTRA_TEXT, main_textview.getText())

        // Make sure the provider knows
        // it should work with that Intent
        shareActionProvider?.setShareIntent(shareIntent)
        shareActionProvider
    }

    fun displayWelcome() {
        sharedPreferences = getSharedPreferences(PREFS, Context.MODE_PRIVATE)
        name = sharedPreferences!!.getString(PREF_NAME, "")
        if (name.length > 0) run {

            // If the name is valid, display a Toast welcoming them
            Toast.makeText(this, "Welcome back, $name!", Toast.LENGTH_LONG).show()
        }
        else run {

            val alert = AlertDialog.Builder(this)
            alert.setTitle("Hello!")
            alert.setMessage("What is your name?")

            val input = EditText(this)
            alert.setView(input)

            alert.setPositiveButton(
                "OK"
            ) { dialog, whichButton ->
                val inputName = input.text.toString()

                val e = sharedPreferences!!.edit()
                e!!.putString(PREF_NAME, inputName)
                e!!.apply()

                Toast.makeText(applicationContext, "Welcome, $inputName!", Toast.LENGTH_LONG).show()
            }

            alert.setNegativeButton("Cancel") { dialog, whichButton -> }
            alert.show()
        }
    }

    private fun queryBooks(searchString: String) {
//        Toast.makeText(this, "Welcome back, $searchString!", Toast.LENGTH_LONG).show()
        // Prepare your search string to be put in a URL
        // It might have reserved characters or something
        var urlString = ""
        try {
            urlString = URLEncoder.encode(searchString, "UTF-8")
        } catch (e: UnsupportedEncodingException) {

            // if this fails for some reason, let the user know why
            e.printStackTrace()
            Toast.makeText(this, "Error: " + e.message, Toast.LENGTH_LONG).show()
        }


        // Create a client to perform networking
        val client = AsyncHttpClient()

        // 11. start progress bar
        setProgressBarIndeterminateVisibility(true)

        // Have the client get a JSONArray of data
        // and define how to respond
        client.get(QUERY_URL + urlString, object : JsonHttpResponseHandler() {

            override fun onSuccess(jsonObject: JSONObject?) {

                // 11. stop progress bar
                setProgressBarIndeterminateVisibility(true)

                // Display a "Toast" message
                // to announce your success
                Toast.makeText(applicationContext, "Success!", Toast.LENGTH_LONG).show()

                // update the data in your custom method.
//                var jsonAdapter: JSONAdapter
                jsonAdapter?.UpdateData(jsonObject?.optJSONArray("docs"))
            }

            override fun onFailure(statusCode: Int, throwable: Throwable, error: JSONObject) {

                // 11. stop progress bar
                setProgressBarIndeterminateVisibility(false)

                // Display a "Toast" message
                // to announce the failure
                Toast.makeText(
                    applicationContext, "Error: " + statusCode + " "
                            + throwable.message, Toast.LENGTH_LONG
                ).show()

                // Log error message
                // to help solve any problems
                Log.e("omg android", statusCode.toString() + " " + throwable.message)
            }
        })
    }

}
